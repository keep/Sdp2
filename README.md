# Sdp
Sdp2:Simple Docker PaaS, Version2

代码由C Python Shell PHP编写而成，用户层通过WEB触发、查看自己的信息和服务，其他没有特别需求的通过CLI方式，具体模块功能见底部。
下面两幅图依次为Sdp思维图和架构图：

![Sdp 思维](docs/Sdp2思维导图.png)

![Sdp 架构](docs/Sdp2架构图.png)



##【Sdp核心组件为模块或API，为命令行或WEB方式】

SUC,Sdp User Center={用户中心;WEB}

SDT,Sdp Docker Trigger={触发docker;CLI}

SDC2,Sdp Docker Console Center={Docker管理中心;WEB}

SR,Sdp Router={路由系统;CLI}

SNC2,Sdp Node Console Center={节点控制中心;WEB+CLI}

SCM,Sdp Code Manager={容器内代码管理;CLI}

SNIS,Sdp Node Information Synchronous={节点信息同步系统;CLI}

