#global functions for shell

function checknet()   
{  
    timeout=5  #超时时间 
    target=www.baidu.com  #目标网站 
    ret_code=`curl -I -s --connect-timeout $timeout $target -w %{http_code} | tail -n1`  #获取响应状态码 
    if [ "x$ret_code" = "x200" ]; then  
        return 0
    else  
        return 1
    fi
}
