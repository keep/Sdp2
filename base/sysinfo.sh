#!/bin/bash 
#This script is used to check the server
clear
#system info 
system_info() { 
echo "**********************************************" 
echo "system info:" 
echo "   System-release : `cat /etc/redhat-release`" 
echo "   Kernel-release : `uname -a|awk '{print $1,$3}'`" 
echo "   Server-Model : `dmidecode | grep "Product Name:"|sed -n '1p'|awk -F': ' '{print $2}'`" 
} 

#CPU info 
cpu_info() { 
echo "**********************************************" 
echo "CPU info:" 
echo "    Frequency : `cat /proc/cpuinfo | grep "model name" | uniq |awk -F': ' '{print $2}'`" 
echo "    CPU cores:  `cat /proc/cpuinfo | grep "cpu cores" | uniq |awk -F': ' '{print $2}'`" 
echo "    Logic Count : `cat /proc/cpuinfo | grep "processor" | sort -u| wc -l `" 
echo "    Physical Count : `cat /proc/cpuinfo | grep "physical" | sort -u| wc -l`" 
echo "    Cache size : `cat /proc/cpuinfo| grep "cache size"|uniq|awk '{print $4,$5}'`" 
} 
 
#memory info 
mem_info() { 
memory=`dmidecode |grep "Range Size"|head -1|awk '{print $3$4}'` 
mem_size=`echo "This server has ${memory} memory."` 
echo "**********************************************" 
echo "Memory info:" 
echo "   Total : ${mem_size}" 
echo "   Count : `dmidecode |grep -A16 "Memory Device$"|grep Size|awk '{if($2!~/No/) print $0}'|wc -l`" 
dmidecode |grep -A20 "Memory Device$"|grep Size|sed '{s/^       */   /g};{/No/d}' 
} 
 
#disk and partitions 
swap_pos=`cat /proc/swaps|sed -n '2p'|awk '{print $1}'` 
partition_info() { 
echo "**********************************************" 
echo "Hard disk info:" 
echo "`fdisk -l|grep Disk|awk -F, '{print $1}'`" 
echo "**********************************************" 
echo "Partition info:" 
echo 
df -h | grep -v Filesystem | sed "s:none:${swap_pos}:" 
echo 
} 
 
#network adapter info for CentOS6.x
adapter_info() {
duplex_eth0=`ethtool eth0 | grep Duplex | awk '{if($2~/Full/) print "Full"};{if($2~/Half/)print "Half"};{if($2~/Uknown!/) print "unknown"}'` 
Negotiation_eth0=`ethtool eth0 | grep "Advertised auto-negotiation"|awk -F': ' '{if($2~/No/) print "Non-negotiation."};{if($2~/Yes/) print "Negotiation"}'` 
IP_eth0=`cat /etc/sysconfig/network-scripts/ifcfg-eth0|grep IPADDR|awk -F= '{print $2}'` 
speed_eth0=`ethtool eth0|grep Speed|awk '{print $2}'` 
if [ -f /etc/sysconfig/network-scripts/ifcfg-eth1 ]; then
duplex_eth1=`ethtool eth1 | grep Duplex | awk '{if($2~/Full/) print "Full"};{if($2~/Half/)print "Half"};{if($2~/Uknown!/) print "unknown"}'`
Negotiation_eth1=`ethtool eth1 | grep "Advertised auto-negotiation"|awk -F': ' '{if($2~/No/) print "Non-negotiation"};{if($2~/Yes/) print "Negotiation"}'`
IP_eth1=`cat /etc/sysconfig/network-scripts/ifcfg-eth1|grep IPADDR|awk -F= '{print $2}'`
speed_eth1=`ethtool eth1|grep Speed|awk '{print $2}'`
echo "**********************************************" 
echo "Network adapter info:" 
echo  
echo "  IP_eth0 : ${IP_eth0}        IP_eth1 : ${IP_eth1}" 
echo "  Speed_eth0 : ${speed_eth0}          Speed_eth1 : ${speed_eth1}" 
echo "  Duplex_eth0 : ${duplex_eth0}            Duplex_eth1 : ${duplex_eth1}" 
echo "  Negotiation_eth0 : ${Negotiation_eth0}  Negotiation_eth1 : ${Negotiation_eth1}" 
echo "**********************************************"
else
echo "**********************************************" 
echo "Network adapter info:" 
echo  
echo "  IP_eth0 : ${IP_eth0}" 
echo "  Speed_eth0 : ${speed_eth0}" 
echo "  Duplex_eth0 : ${duplex_eth0}" 
echo "  Negotiation_eth0 : ${Negotiation_eth0}" 
echo "**********************************************"
fi
}

PROCESS=$(expr `ps aux | grep -v "grep" | grep -v wc | wc -l` - 2)
SERVER_IP=$(echo $SSH_CONNECTION | awk '{print $3}')
CLIENT_IP=$(echo $SSH_CONNECTION | awk '{print $1}')
USERS=$(who | wc -l)
ROOT_USERD=$(df -h | grep "/$" | awk '{print $3}')
ROOT_Surplus=$(df -h | grep "/$" | awk '{print $4}')
ROOT_UAGE=$(df -h | grep "/$" | awk '{print $5}')
system_info 
cpu_info 
mem_info 
adapter_info

echo -e "\033[31mSystem Process: $PROCESS\033[0m"
echo -e "\033[32mServer IP: $SERVER_IP\033[0m"
echo -e "\033[32mClient IP: $CLIENT_IP\033[0m"
echo -e "\033[35mLogin Users: ${USERS}\033[0m"
echo -e "\033[34m根分区使用情况:\033[0m"
echo "    已用大小：$ROOT_USERD"
echo "    剩余大小：$ROOT_Surplus"
echo "    使用百分比：$ROOT_UAGE"

