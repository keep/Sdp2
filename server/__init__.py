#-*- coding=utf-8 -*-
#sys.path.append(script path and conf node server)
'''	
@author:'saintic.com'
@comments:初始化服务器端，包括基本系统、控制系统、健康监测系统。
'''

import os,sys,commands
from GroupInfo import nodes
from GroupInfo import node_services

ROOT=sys.path[0]
os.environ['ROOT']=str(ROOT)

def docker_check():
    '''docker check or install'''
    __docker_exist=commands.getstatusoutput('which docker')[0]
    if __docker_exist == 0:
        print "Docker has been installed!"
    else:
        __docker_install=os.system("sh $ROOT/../base/docker.sh")
        if __docker_install == 0:
            return "Docker has been installed successfully!"
        else:
            return "Please Check the Script:", ROOT + "/../base/docker.sh"

def nginx_check():
    '''nginx service'''
    if commands.getstatusoutput('which nginx')[0] == 0:
        print "Nginx has been installed!"
    elif commands.getstatusoutput('ps aux | grep -v grep | grep nginx')[0] == 0:
        print "Nginx has been installed!"
    else:
        print "未安装Nginx服务，可能的原因是搜索不到nginx命令或尚未安装Nginx服务!"
        print '将执行Nginx安装脚本'
        __nginx_install=os.system("sh $ROOT/../base/nginx.sh")
        if __nginx_install == 0:
            return "Nginx has been installed successfully!"
        else:
            return "Please Check the Script:", ROOT + "/../base/nginx.sh"

def GetSysInfo(*server,*nodes):
    '''node info ; server info.'''


        






