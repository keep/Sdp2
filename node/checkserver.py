#coding:utf-8
'''
@author:saintic.com
1.判断服务器网络是否正常
2.判断服务器docker是否存活
'''
import os,sys,socket,subprocess

from GroupInfo import server
from GroupInfo import server_services

#把程序输出定位到/dev/null,否则会在程序运行时会在标准输出中显示命令的运行信息
def servercheck(server):
    with open(os.devnull,'w') as fnull:
        result = subprocess.call('ping '+server+' -c 2', shell = True, stdout = fnull, stderr = fnull)
        if result:
            print 'Server Connected failure'
        else:
            print 'Server Connected success'
    return

def servicecheck(server_services):
    for ip,port in server_services.items():
        sk = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sk.settimeout(1)
        try:
            sk.connect((ip,port))
            print '%s %d service is UP!' %(ip,port)
        except Exception:
            print '%s %d service is DOWN!' %(ip,port)
        finally:
            sk.close()
    return

if __name__ == '__main__':
    servercheck(server)
    servicecheck(server_services)
